# mySchedule #

## What is mySchedule? ##
> ***MySchedule is your best interactive web app for creating school plan, or even your personal calendar.***

> ***It will always remind you of closest events! Just register and try it as soon as possible!***


### What tools did we use? ###

* Django 1.10
* Django tables 2
* Django REST framework
* PostgreSQL
* Bootstrap
* Sass
* jQuery

### Who created this project? ###

* Jakub Walat (developer)
* Jakub Stasiak (developer)