from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponseRedirect, Http404
from .models import Article
from .forms import ArticleForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


# Create your views here.
def article_create(request):
    if not request.user.is_staff or not request.user.is_superuser:
        raise Http404
    form = ArticleForm(request.POST or None, request.FILES or None, auto_id="%s")

    if form.is_valid():
        title = form.cleaned_data.get("title")
        text = form.cleaned_data.get("text")
        image = form.cleaned_data.get("image")
        important = form.cleaned_data.get("important")

        instance = Article.objects.create(title=title,
                                          text=text,
                                          image=image,
                                          important=important)
        instance.save()

        return HttpResponseRedirect(instance.get_absolute_path())

    context = {
        "form": form,
    }
    return render(request, "article_form.html", context)


def article_detail(request, slug=None):
    instance = get_object_or_404(Article, slug=slug)
    context = {
        "title": instance.title,
        "instance": instance,
    }
    return render(request, "article_detail.html", context)


def article_list(request):
    articles_list = Article.objects.all()  # .order_by("-time_stamp")
    paginator = Paginator(articles_list, 3)  # Show 25 contacts per page
    page_request = "page"

    page = request.GET.get(page_request)
    try:
        queryset = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        queryset = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        queryset = paginator.page(paginator.num_pages)

    context = {
        "object_list": queryset,
        "title": "List of all articles",
        "page_request": page_request
    }
    return render(request, "article_list.html", context)


def article_update(request, slug=None):
    if not request.user.is_staff or not request.user.is_superuser:
        raise Http404
    instance = get_object_or_404(Article, slug=slug)
    form = ArticleForm(request.POST or None, request.FILES or None, instance=instance, auto_id = "%s")

    if form.is_valid():
        instance.save()
        return HttpResponseRedirect(instance.get_absolute_path())

    context = {
        "title": instance.title,
        "instance": instance,
        "form": form,
    }
    return render(request, "article_form.html", context)


def article_delete(request, slug=None):
    if not request.user.is_staff or not request.user.is_superuser:
        raise Http404
    instance = get_object_or_404(Article, slug=slug)
    instance.delete()
    return redirect("articles:list")
