from django.db import models
from django.core.urlresolvers import reverse
from django.db.models.signals import pre_save
from django.utils.text import slugify
import uuid


def upload_location(instance, filename):
    return "{}/{}".format(instance.slug, filename)


# Create your models here.
class Article(models.Model):
    image = models.ImageField(upload_to=upload_location,
                              null=True, blank=True,
                              height_field="height",
                              width_field="width")
    height = models.IntegerField(default=0)
    width = models.IntegerField(default=0)
    title = models.CharField(max_length=120)
    slug = models.SlugField(unique=True, default=uuid.uuid4)
    text = models.TextField()
    last_updated = models.DateTimeField(auto_now=True)
    time_stamp = models.DateTimeField(auto_now_add=True)
    important = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    def get_absolute_path(self):
        return reverse("articles:detail", kwargs={"slug": self.slug})

    class Meta:
        ordering = ["-id"]


def create_slug(instance, new_slug=None):
    slug = slugify(instance.title)
    if new_slug is not None:
        slug = new_slug
    qs = Article.objects.filter(slug=slug)
    exists = qs.exists()
    if exists:
        new_slug = "{}-{}".format(slug, qs.first().id)
        return create_slug(instance, new_slug=new_slug)
    return slug


def pre_save_post_receiver(sender, instance, *args, **kwargs):
        instance.slug = create_slug(instance=instance)


pre_save.connect(pre_save_post_receiver, sender=Article)
