from django import forms
# from .models import Article


class ArticleForm(forms.Form):
    error_css_class = 'form-error'

    title = forms.CharField(widget=forms.TextInput(attrs={
        "class": "form-title",
    }))
    text = forms.CharField(widget=forms.Textarea(attrs={
        "class": "form-text"
    }))
    image = forms.ImageField(required=False)
    important = forms.BooleanField(required=False, widget=forms.CheckboxInput(attrs={
        "class": "form-important"
    }))

    def is_valid(self):
        valid = super(ArticleForm, self).is_valid()

        if not valid:
            return valid

        if not self.cleaned_data["title"] or not self.cleaned_data["text"]:
            raise forms.ValidationError("Field cannot be empty!")
            return False
        else:
            return True


# class ArticleForm(forms.ModelForm):
#     class Meta:
#         model = Article
#         fields = [
#             "title",
#             "image",
#             "text",
#         ]
