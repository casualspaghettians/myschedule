/*jslint browser: true, devel: true */
(function ($) {
    "use strict";
    $(document).ready(function () {
        function nav_show() {
            var isClosed = false;

            if (isClosed === true) {
                $(".overlay").hide();
                isClosed = false;
            } else {
                $(".overlay").show();
                isClosed = true;
            }

            trigger.click(function () {
                nav_show();
            });
        }

        $("[data-toggle='offcanvas']").click(function () {
            $("#wrapper").toggleClass('toggled');
        });
    });
}(jQuery));